# README #

Application to display and record a simulation

### Database format to respect ###

* All values are separated with a space
* First line : FPS
* Then each line is a frame where the positions of the particles are defined like this :
x1 y1 z1 x2 y2 z2 ...

An example is the file [abc.db](https://bitbucket.org/JulV/oglsimdisplayer/src/master/OGLSimDisplayer/abc.db)

### How do I get set up? ###

* Link lwjgl.jar, lwjgl_util.jar and slick.jar to your project
* Create a valid Database file with another code
* Run it

### Who do I talk to? ###

* JulV