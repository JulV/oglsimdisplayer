package com.julvdev.oglsimdisplayer.view;
/**
 * An enumeration of the different states the View can take
 * @author Julien Van Loo
 *
 */
public enum State {
	RUNNING,PAUSE,LOADING;
}
