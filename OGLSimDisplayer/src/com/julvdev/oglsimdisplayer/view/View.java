package com.julvdev.oglsimdisplayer.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Dimension;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;

import com.julvdev.objloaderogl.ModelVBO;
import com.julvdev.objloaderogl.OBJLoaderOGL;
import com.julvdev.oglsimdisplayer.controller.InputChecker;
import com.julvdev.oglsimdisplayer.model.Database;
import com.julvdev.oglsimdisplayer.view.camera.Camera;
import com.julvdev.oglsimdisplayer.view.camera.EulerCamera;
/**
 * The view of OGLSimDisplay
 * @author Julien Van Loo
 *
 */
public class View {
	
	private static Camera camera;
	
	private Dimension resolution;
	private State state=State.RUNNING;
	private int timeSpeed=10, frameNumber, FPS=60;
	private ModelVBO particle;
	private InputChecker controller;
	
	public boolean isRecording = false;
	/**
	 * Constructs the view
	 * @param resolution The resolution of the view
	 * @param objFile The obj file to use for the particles
	 */
	public View(Dimension resolution, File objFile) {
		this.resolution = resolution;
		controller = new InputChecker(this);
		setUpDisplay();
		setUpOpenGL();
		setUpLighting();
		setUpCamera();
		loadModel(objFile);
	}
	/**
	 * Shows the view
	 * @param data The database to show
	 */
	public void show(Database data) {
		int i=0;
		FPS=data.getFPS();
		while (!Display.isCloseRequested()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	        GL11.glLoadIdentity();
	        camera.applyTranslations();
	        set3DMode();
	        for (int j=0; j<data.getFrame(i).size(); j++) {
	        	particle.render(data.getParticleInFrame(i, j), new Vector3f(0f,1f,0.5f), 30);
	        }
	        GL11.glLoadIdentity();
	        set2DMode();
	        GL11.glBegin(GL11.GL_TRIANGLES);  // head up display here
	        GL11.glVertex2f(1, 0.7f);
	        GL11.glVertex2f(1, 1);
	        GL11.glVertex2f(0, 1);
	        GL11.glEnd();
	        controller.checkInput();
	        if (getState() == State.RUNNING) {
	        	if (i<data.getFrameNbr()-1) {
	        		i++;
	        	} else {
	        		i=0;
	        	}
	        }
            Display.update();
            takeFrame();
            Display.sync((int)(data.getFPS()*timeSpeed/10));
		}
		cleanUp();
	}
	/**
	 * Set the display
	 */
	private void setUpDisplay() {
		Display.setVSyncEnabled(true);
        Display.setTitle("OpenGL Simulations Displayer");
		try {
			setUpFullscreen();
        } catch (LWJGLException | NullPointerException e) {
        	System.err.println("Fullscreen not supported!");
			try {
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				System.out.println("Supported fullscreen resolutions are :");
				for (int i=0; i<modes.length; i++) {
	        		if (modes[i].isFullscreenCapable()) {
	        			System.out.println("- "+modes[i].getWidth()+"x"+modes[i].getHeight()+" @ "+modes[i].getFrequency()+"Hz");
	        		}
	        	}
			} catch (LWJGLException e2) {
				System.err.println("Unable to make the compatible fullscreen resolutions list!");
			}
        	System.out.println("Trying to display in a window...");
        	try {
				Display.setDisplayMode(new DisplayMode(resolution.getWidth(), resolution.getHeight()));
				Display.setFullscreen(false);
	            Display.create();
			} catch (LWJGLException e1) {
				System.err.println("The display wasn't initialized correctly. :(");
	            Display.destroy();
	            System.exit(1);
			}
        }
	}
	/**
	 * Set the fullscreen mode
	 * @throws LWJGLException Cannot create the display or load the available display mode
	 * @throws NullPointerException If no fullscreen mode compatible with the screen
	 */
	private void setUpFullscreen() throws LWJGLException, NullPointerException {
		DisplayMode displayMode = null;
		DisplayMode[] modes = Display.getAvailableDisplayModes();
        for (int i = 0; i < modes.length; i++) {
        	if (modes[i].getWidth() == resolution.getWidth() && modes[i].getHeight() == resolution.getHeight() && modes[i].isFullscreenCapable()) {
        		displayMode = modes[i];
        	}
        }
        Display.setDisplayMode(displayMode);
        Display.setFullscreen(true);
        Display.create();
	}
	/**
	 * Set up some OpenGL features
	 */
	private void setUpOpenGL() {
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	    GL11.glShadeModel(GL11.GL_SMOOTH);
	    
	    GL11.glEnable(GL11.GL_CULL_FACE);             // Cull fait que OpenGL ne dessine pas la partie non visible des objets, accélère les rendus
        GL11.glCullFace(GL11.GL_BACK);                // On explique que c'est la face de derrière qu'on ne veut pas dessiner

	    GL11.glEnable(GL11.GL_COLOR_MATERIAL);
	    GL11.glColorMaterial(GL11.GL_FRONT, GL11.GL_DIFFUSE);
	    GL11.glEnable(GL11.GL_BLEND);                 // Activate alpha color (use slightly more resources)
	    

	    GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
	    GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
	    GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
	    
	    // Code the draw the splashscreen (if needed)
	}
	/**
	 * Set up the lighting
	 */
	private void setUpLighting() {
	    GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, floatBuffer(1.0f, 1.0f, 1.0f, 1.0f));
	    GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, 25.0f);

	    GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, floatBuffer(-5.0f, 5.0f, 15.0f, 0.0f));

	    GL11.glLight(GL11.GL_LIGHT0, GL11.GL_SPECULAR, floatBuffer(1.0f, 1.0f, 1.0f, 1.0f));
	    GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, floatBuffer(1.0f, 1.0f, 1.0f, 1.0f));

	    GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, floatBuffer(0.1f, 0.1f, 0.1f, 1.0f));
	}
	/**
	 * Set OpenGL to draw in 2D
	 */
	private void set2DMode() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
	    GL11.glLoadIdentity();
	    //GL11.glOrtho(0.0f, resolution.getWidth(), resolution.getHeight(), 0.0f, -1.0f, 1.0f);
	    GL11.glMatrixMode(GL11.GL_MODELVIEW);
	    GL11.glDisable(GL11.GL_DEPTH_TEST);
	    GL11.glDisable(GL11.GL_LIGHTING);
	    GL11.glDisable(GL11.GL_LIGHT0);
	}
	/**
	 * Set OpenGL to draw in 3D
	 */
	private void set3DMode() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
	    GL11.glLoadIdentity(); // Reset The Projection Matrix
	    GLU.gluPerspective(45.0f, ((float) resolution.getWidth() / (float) resolution.getHeight()), 0.1f, 100.0f);
	    GL11.glMatrixMode(GL11.GL_MODELVIEW);
	    //GLU.gluLookAt(0.0f, 0.0f, 50.0f, 20.0f, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_LIGHTING);
	    GL11.glEnable(GL11.GL_LIGHT0);
	}
	/**
	 * Set the camera
	 */
	private static void setUpCamera() {
        camera = new EulerCamera.Builder().setAspectRatio((float) Display.getWidth() / Display.getHeight()).setRotation(-1.12f, 0.16f, 0f).setPosition(-1.38f, 1.36f, 7.95f).setFieldOfView(60).build();
        camera.applyOptimalStates();
        camera.applyPerspectiveMatrix();
    }
	/**
	 * Destroy the display
	 */
	private void cleanUp() {
		System.out.println("Cleaning the view");
		Display.destroy();
	}
	/**
	 * Create a FloatBuffer of 4 float
	 * @param a float 1
	 * @param b float 2
	 * @param c float 3
	 * @param d float 4
	 * @return A FloatBuffer
	 */
	public static FloatBuffer floatBuffer(float a, float b, float c, float d) {
		float[] data = new float[]{a,b,c,d};
	    FloatBuffer fb = BufferUtils.createFloatBuffer(data.length);
	    fb.put(data);
	    fb.flip();
	    return fb;
	}
	/**
	 * Load the model from the obj file
	 * @param file An obj file
	 */
	private void loadModel(File file) {
		try {
			particle = OBJLoaderOGL.loadModelVBO(file);
		} catch (IOException e) {
			System.err.println("Cannot load the file : "+file.getAbsolutePath());
			e.printStackTrace();
			System.exit(1);
		}
	}
	/**
	 * Set the State of the View
	 * @param state A State
	 * @see State
	 */
	public void setState(State state) {
		this.state = state;
	}
	/**
	 * Get the State of the View
	 * @return A State
	 */
	public State getState() {
		return state;
	}
	/**
	 * Get the speed factor of the animation (10 = normal)
	 * @return An int
	 */
	public int getTimeSpeed() {
		return timeSpeed;
	}
	/**
	 * Set the speed factor of the animation (max=100, min=1, normal=10)
	 * @param value An int
	 */
	public void setTimeSpeed(int value) {
		if (value < 1) {
			value=1;
		} else if (value > 100) {
			value=100;
		}
		timeSpeed=value;
	}
	/**
	 * Get the camera
	 * @return A Camera
	 */
	public Camera getCamera() {
		return camera;
	}
	/**
	 * If recording is on, takes the current displayed frame and add it to the video file
	 */
	public void takeFrame() {
		if (isRecording && state == State.RUNNING) {
			// take the frame and add it in the video file
			GL11.glReadBuffer(GL11.GL_FRONT);
			int width = Display.getDisplayMode().getWidth();
			int height= Display.getDisplayMode().getHeight();
			int bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
			ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
			GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );
			
			new String();
			File file = new File("REC/RecTmp/"+String.valueOf(frameNumber)+".jpg"); // The file to save to.
			String format = "JPG"; // Example: "PNG" or "JPG"
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			   
			for(int x = 0; x < width; x++) 
			{
			    for(int y = 0; y < height; y++)
			    {
			        int i = (x + (width * y)) * bpp;
			        int r = buffer.get(i) & 0xFF;
			        int g = buffer.get(i + 1) & 0xFF;
			        int b = buffer.get(i + 2) & 0xFF;
			        image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
			    }
			}
			   
			try {
			    ImageIO.write(image, format, file);
			} catch (IOException e) { e.printStackTrace(); }
			frameNumber++;
		}
	}
	
	public void intRecord() {
		frameNumber=1;
	}
	
	public void makeVideo() {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-kk-mm-ss");
			Process p = Runtime.getRuntime().exec("ffmpeg -framerate "+FPS+" -start_number 1 -i REC/RecTmp/%d.jpg -vcodec mpeg4 REC/"+dateFormat.format(new Date())+".avi");
			System.out.println("Video is encoding...");
			p.waitFor();
			File tempDir = new File("REC/RecTmp");
			File files[] = tempDir.listFiles();
			for (File file : files) {
				file.delete();
			}
		} catch (IOException | InterruptedException e) {
			System.err.println("Couldn't create the video stream");
		}
	}
}
