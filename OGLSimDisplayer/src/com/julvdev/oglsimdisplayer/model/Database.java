package com.julvdev.oglsimdisplayer.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;
/**
 * Object that contains the datas for the simulation
 * @author Julien Van Loo
 *
 */
public class Database {
	
	private int fps;
	private ArrayList<ArrayList<Vector3f>> datas = new ArrayList<ArrayList<Vector3f>>();
	/**
	 * Constructs the Database object (Need to execute the load function to load a file)
	 * @see #load(File)
	 */
	public Database() {
		
	}
	/**
	 * Loads the file in the Database object
	 * @param database The file to load
	 * @throws IOException if the file cannot be read
	 */
	public void load(File database) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(database));
		String line = reader.readLine();
		String[] values = line.split(" ");
		fps = Integer.parseInt(values[0]);
		while ((line = reader.readLine()) != null) {
			datas.add(new ArrayList<Vector3f>());
			values = line.split(" ");
			for (int i=0; i<values.length; i=i+3) {
				datas.get(datas.size()-1).add(new Vector3f(Float.parseFloat(values[i]), Float.parseFloat(values[i+1]), Float.parseFloat(values[i+2])));
			}
		}
		reader.close();
	}
	/**
	 * Get the framerate of the animation
	 * @return An int
	 */
	public int getFPS() {
		return fps;
	}
	/**
	 * Get a given frame
	 * @param index The index of the frame
	 * @return An ArrayList of org.lwjgl.util.vector.Vector3f
	 */
	public ArrayList<Vector3f> getFrame(int index) {
		return datas.get(index);
	}
	/**
	 * Get the count of frames
	 * @return An int
	 */
	public int getFrameNbr() {
		return datas.size();
	}
	/**
	 * Get a particle in a given frame
	 * @param frame The frame index
	 * @param particle The particle index
	 * @return A org.lwjgl.util.vector.Vector3f
	 */
	public Vector3f getParticleInFrame(int frame, int particle) {
		return datas.get(frame).get(particle);
	}
}
