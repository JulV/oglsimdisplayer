package com.julvdev.oglsimdisplayer;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import org.lwjgl.util.Dimension;

import com.julvdev.oglsimdisplayer.model.Database;
import com.julvdev.oglsimdisplayer.view.View;
/**
 * Main class of OGLSimDisplayer
 * @author Julien Van Loo
 *
 */
public class OGLSimDisplayer {
	/**
	 * First method called
	 * @param args Args given to the application
	 */
	public static void main(String[] args) {
		JFileChooser chooser = new JFileChooser();
		if (args.length > 0) {
			chooser.setSelectedFile(new File(args[0]));
		} else {
			chooser.setDialogTitle("database selection");
			int returnValue = chooser.showOpenDialog(null);
			if (returnValue != JFileChooser.APPROVE_OPTION) {
				System.out.println("No file selected!");
				System.exit(0);
			}
		}
        Database database = new Database();
        System.out.println("Loading database...");
        try {
			database.load(chooser.getSelectedFile());
		} catch (IOException e) {
			System.err.println("Cannot find the file : "+chooser.getSelectedFile().getAbsolutePath());
		}
        System.out.println("Creating the view");
        View test = new View(new Dimension(1360, 768), new File("cube.obj"));
        test.show(database);
        System.out.println("exit");
	}

}
