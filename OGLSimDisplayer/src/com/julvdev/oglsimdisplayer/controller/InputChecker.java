package com.julvdev.oglsimdisplayer.controller;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.julvdev.oglsimdisplayer.view.State;
import com.julvdev.oglsimdisplayer.view.View;
/**
 * Controller of the OGLSimDisplayer
 * @author Julien Van Loo
 *
 */
public class InputChecker {
	
	private View gui;
	private long lastKeyTime=0;
	/**
	 * Constructor
	 * @param gui The view that must be controlled
	 */
	public InputChecker(View gui) {
		this.gui = gui;
	}
	/**
	 * Check the inputs (Keyboard/Mouse)
	 */
	public void checkInput() {
		gui.getCamera().processMouse(1, 80, -80);
        gui.getCamera().processKeyboard(16, 10, 10, 10);
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			System.out.println("Escaped");
			Display.destroy();
			System.exit(0);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_ADD) && System.currentTimeMillis()-lastKeyTime > 100) {
			lastKeyTime = System.currentTimeMillis();
			gui.setTimeSpeed(gui.getTimeSpeed()+1);
			System.out.println("Speed : "+gui.getTimeSpeed());
		} else if (Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT) && System.currentTimeMillis()-lastKeyTime > 100) {
			lastKeyTime = System.currentTimeMillis();
			gui.setTimeSpeed(gui.getTimeSpeed()-1);
			System.out.println("Speed : "+gui.getTimeSpeed());
		} else if (Keyboard.isKeyDown(Keyboard.KEY_R) && System.currentTimeMillis()-lastKeyTime > 500) {
			lastKeyTime = System.currentTimeMillis();
			if (gui.isRecording) {
				gui.isRecording=false;
				gui.makeVideo();
				System.out.println("Recording stopped");
			} else {
				gui.intRecord();
				gui.isRecording=true;
				System.out.println("Recording started");
			}
		} else if (Keyboard.isKeyDown(Keyboard.KEY_SPACE) && System.currentTimeMillis()-lastKeyTime > 500) {
			lastKeyTime = System.currentTimeMillis();
			if (gui.getState() == State.RUNNING) {
				gui.setState(State.PAUSE);
			} else if (gui.getState() == State.PAUSE) {
				gui.setState(State.RUNNING);
			}
		}
		
		if (Mouse.isButtonDown(0)) {
            Mouse.setGrabbed(true);
        } else if (Mouse.isButtonDown(1)) {
            Mouse.setGrabbed(false);
        }
	}
}
