package com.julvdev.objloaderogl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;


public class Material {
	private Vector3f diffuse;   		  // Couleur de la diffusion (3 composantes), c'est pas un vecteur
	private float alpha;       		  // Transparence du matériau
	private String name;        		  // Nom du matériau
	private Texture texture;    		  // Texture du matériau
	private boolean isTextured=false;  // Indique si le matériau est texturé
	
	public Material(String name) {
		diffuse = new Vector3f(1,1,1);
		alpha = 1.0f;
		this.name=name;
		texture=null;
	}
	
	public Vector3f getDiffuse() {
		return diffuse;
	}
	
	public void setDiffuse(Vector3f diffuse) {
		this.diffuse = diffuse;
	}
	
	public float getTransparency() {
		return alpha;
	}
	
	public void setTransparency(float transparency) {
		this.alpha=transparency;
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public void setTexture(String filename) {
		try {
			this.texture = TextureLoader.getTexture(filename.split("\\.")[1].toUpperCase(), new FileInputStream(new File(filename)));
			this.isTextured=true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isTextured() {
		return isTextured;
	}
	
	public String getName() {
		return name;
	}
}
