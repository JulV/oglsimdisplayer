package com.julvdev.objloaderogl;
import org.lwjgl.util.vector.Vector3f;


public class Face {
	private Vector3f vertex = new Vector3f();  // C'est juste pour stocker 3 indices, c'est pas vraiment un vecteur
	private Vector3f normal =  new Vector3f(); // Idem mais pour les normales
	private Vector3f texVertex = new Vector3f(); // Idem pour les positions de texture
	private Material material;                 // Matériel de la face, sa texture
	
	public Face(Vector3f vertex, Vector3f normal, Material material) {
		this.vertex=vertex;
		this.normal=normal;
		this.material=material;
		
		if (material == null) {                // Si il n'y a pas de matériau, on en map un par défaut pour éviter tout plantage
			material = new Material("Default");
		}
	}
	
	public Face(Vector3f vertex, Vector3f normal, Vector3f texVertex, Material material) {
		this(vertex, normal, material);
		this.texVertex=texVertex;
	}
	
	public int[] getVertexIndices() {
		int[] indices = {(int)vertex.x,(int)vertex.y,(int)vertex.z};
		return indices;
	}
	
	public int[] getNormalIndices() {
		int[] indices = {(int)normal.x,(int)normal.y,(int)normal.z};
		return indices;
	}
	
	public int[] getTexVertexIndices() {
		int[] indices = {(int)texVertex.x,(int)texVertex.y,(int)texVertex.z};
		return indices;
	}
	
	public Material getMaterial() {
		return material;
	}
}
