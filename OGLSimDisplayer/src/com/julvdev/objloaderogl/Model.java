package com.julvdev.objloaderogl;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Model {
	private List<Vector3f> vertices = new ArrayList<Vector3f>();
	private List<Vector3f> normals = new ArrayList<Vector3f>();
	private List<Vector2f> texVertices = new ArrayList<Vector2f>();
	private List<Face> faces = new ArrayList<Face>();
	private List<Material> materials = new ArrayList<Material>();
	
	public Model() {
		
	}
	
	public List<Vector3f> getVertices() {
		return vertices;
	}
	
	public List<Vector3f> getNormals() {
		return normals;
	}
	
	public List<Vector2f> getTexVertices() {
		return texVertices;
	}
	
	public List<Face> getFaces() {
		return faces;
	}
	
	public List<Material> getMaterials() {
		return materials;
	}
	
	public void render(Vector3f position, Vector3f rotDir, float angle) {
		GL11.glPushMatrix();
		GL11.glTranslatef(position.x, position.y, position.z); // On place l'objet par rapport au reste de la scène
		GL11.glRotatef(angle, rotDir.x, rotDir.y, rotDir.z);
		GL11.glBegin(GL11.GL_TRIANGLES);
        for (Face face : this.getFaces()) {
        	Vector3f n1 = this.getNormals().get(face.getNormalIndices()[0] - 1);   // On récupère les vecteurs et normales
        	Vector3f v1 = this.getVertices().get(face.getVertexIndices()[0] - 1);
            Vector3f n2 = this.getNormals().get(face.getNormalIndices()[1] - 1);
            Vector3f v2 = this.getVertices().get(face.getVertexIndices()[1] - 1);
            Vector3f n3 = this.getNormals().get(face.getNormalIndices()[2] - 1);
            Vector3f v3 = this.getVertices().get(face.getVertexIndices()[2] - 1);
        	GL11.glColor4f(face.getMaterial().getDiffuse().x, face.getMaterial().getDiffuse().y, face.getMaterial().getDiffuse().z, face.getMaterial().getTransparency());  // On met la couleur de la face
        	if (face.getMaterial().isTextured()) {        // Si la face est texturée
        		GL11.glEnable(GL11.GL_TEXTURE_2D);
        		face.getMaterial().getTexture().bind();   // Lier la texture à la face
            	Vector2f vt1 = this.getTexVertices().get(face.getTexVertexIndices()[0] - 1); // On récupère les vecteurs de textures
                Vector2f vt2 = this.getTexVertices().get(face.getTexVertexIndices()[1] - 1);
                Vector2f vt3 = this.getTexVertices().get(face.getTexVertexIndices()[2] - 1);
        		
                GL11.glTexCoord2f(vt1.x,(float)(face.getMaterial().getTexture().getImageHeight())-vt1.y);
                GL11.glNormal3f(n1.x, n1.y, n1.z);
                GL11.glVertex3f(v1.x, v1.y, v1.z);
                GL11.glTexCoord2f(vt2.x,(float)(face.getMaterial().getTexture().getImageHeight())-vt2.y);
                GL11.glNormal3f(n2.x, n2.y, n2.z);
                GL11.glVertex3f(v2.x, v2.y, v2.z);
                GL11.glTexCoord2f(vt3.x,(float)(face.getMaterial().getTexture().getImageHeight())-vt3.y);
                GL11.glNormal3f(n3.x, n3.y, n3.z);
                GL11.glVertex3f(v3.x, v3.y, v3.z);
        	}
        	else {                                        // Si la face n'est pas texturée
        		GL11.glDisable(GL11.GL_TEXTURE_2D);
        		
        		GL11.glNormal3f(n1.x, n1.y, n1.z);
        		GL11.glVertex3f(v1.x, v1.y, v1.z);
        		GL11.glNormal3f(n2.x, n2.y, n2.z);
        		GL11.glVertex3f(v2.x, v2.y, v2.z);
        		GL11.glNormal3f(n3.x, n3.y, n3.z);
        		GL11.glVertex3f(v3.x, v3.y, v3.z);
        	}
        }
        GL11.glEnd();
        GL11.glPopMatrix();
	}
}
