package com.julvdev.objloaderogl;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;


public class OBJLoaderOGL {
	public static Model loadModel(File f) throws FileNotFoundException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader(f));
		Model m = new Model();
		Material material = new Material("Default");
		String line;
		while ((line = reader.readLine()) != null) {       // Parser le fichier obj (et les fichiers de materiaux aussi)
			if (line.startsWith("v ")) {                          // Dans le cas où c'est un vecteur
				float x = Float.valueOf(line.split(" ")[1]);
				float y = Float.valueOf(line.split(" ")[2]);
				float z = Float.valueOf(line.split(" ")[3]);
				m.getVertices().add(new Vector3f(x,y,z));
			} else if (line.startsWith("vn ")) {                 // Dans le cas où c'est une normale
				float x = Float.valueOf(line.split(" ")[1]);
				float y = Float.valueOf(line.split(" ")[2]);
				float z = Float.valueOf(line.split(" ")[3]);
				m.getNormals().add(new Vector3f(x,y,z));
			} else if (line.startsWith("vt ")) {                 // Dans le cas où c'est une texture
				m.getTexVertices().add(new Vector2f(Float.valueOf(line.split(" ")[1]), Float.valueOf(line.split(" ")[2])));
			} else if (line.startsWith("f ")) {                  // Dans le cas où c'est une face
				Vector3f vertexIndices = new Vector3f(Float.valueOf(line.split(" ")[1].split("/")[0]), Float.valueOf(line.split(" ")[2].split("/")[0]), Float.valueOf(line.split(" ")[3].split("/")[0]));
				Vector3f normalIndices = new Vector3f(Float.valueOf(line.split(" ")[1].split("/")[2]), Float.valueOf(line.split(" ")[2].split("/")[2]), Float.valueOf(line.split(" ")[3].split("/")[2]));
				if (line.split(" ")[1].split("/")[1].isEmpty() || line.split(" ")[2].split("/")[1].isEmpty() || line.split(" ")[3].split("/")[1].isEmpty()) {
					m.getFaces().add(new Face(vertexIndices, normalIndices, material));
				} else {
					Vector3f texVertexIndices = new Vector3f(Float.valueOf(line.split(" ")[1].split("/")[1]), Float.valueOf(line.split(" ")[2].split("/")[1]), Float.valueOf(line.split(" ")[3].split("/")[1]));
					m.getFaces().add(new Face(vertexIndices, normalIndices, texVertexIndices, material));
				}
			} else if (line.startsWith("mtllib ")) {             // Dans le cas où c'est l'adresse d'un fichier de matériau
				parseMaterial(m, f.getParent()+"/"+line.replaceAll("mtllib ", "").trim());
			} else if (line.startsWith("usemtl ")) {             // Dans le cas où ça indique le nom du matériau des faces à venir
				for (Material mat : m.getMaterials())
				{
					if (mat.getName().equals(line.replaceAll("usemtl ", "").trim()))
					{
						material = mat;
						break;
					}
				}
				//material = m.getMaterials().get(line.replaceAll("usemtl ", "").trim()); // Méthode trouvée sur intrnet mais je vois pas comment ça marche (HashMap?)
			}
		}
		reader.close();
		
		return m;
	}
	
	private static void parseMaterial(Model m, String name) throws FileNotFoundException, IOException {
		Material material = null;
		BufferedReader reader = new BufferedReader(new FileReader(new File(name)));
		String line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("newmtl ")) {
				if (material != null) {
					m.getMaterials().add(material);  // On ajoute le matériau précédent qui est entièrement chargé
				}
				
				material = new Material(line.split(" ")[1]);  // on crée le nouveau objet material
			} else if (line.startsWith("Kd ")) {    // Donne la diffusion de la couleur du matériau
				material.setDiffuse(new Vector3f(Float.valueOf(line.split(" ")[1]), Float.valueOf(line.split(" ")[2]), Float.valueOf(line.split(" ")[3])));  // Place la valeur de diffusion
			} else if (line.startsWith("d ") || line.startsWith("Tr ")) {     // Donne la transparence du materiau
				material.setTransparency(Float.valueOf(line.split(" ")[1]));
			} else if (line.startsWith("map_Kd ")) {                          // Donne le nom du fichier de texture du matériau
				material.setTexture(line.split(" ")[1]);
			}
		}
		m.getMaterials().add(material);  // On ajoute le dernier matériau qui est prêt
		reader.close();
	}
	
	public static ModelVBO loadModelVBO(File f)  throws FileNotFoundException, IOException {
		Model m = loadModel(f);
		ModelVBO mvbo = new ModelVBO();
		ArrayList<Float> dynamicArray = new ArrayList<Float>();
		
		for (Face face : m.getFaces()) {
			for (int i=0; i<3; i++) {
				dynamicArray.add(m.getVertices().get(face.getVertexIndices()[i]-1).x);
				dynamicArray.add(m.getVertices().get(face.getVertexIndices()[i]-1).y);
				dynamicArray.add(m.getVertices().get(face.getVertexIndices()[i]-1).z);
				dynamicArray.add(m.getNormals().get(face.getNormalIndices()[i]-1).x);
				dynamicArray.add(m.getNormals().get(face.getNormalIndices()[i]-1).y);
				dynamicArray.add(m.getNormals().get(face.getNormalIndices()[i]-1).z);
				dynamicArray.add(face.getMaterial().getDiffuse().x);
				dynamicArray.add(face.getMaterial().getDiffuse().y);
				dynamicArray.add(face.getMaterial().getDiffuse().z);
				dynamicArray.add(face.getMaterial().getTransparency());
			}
		}
		
		float[] vertexDataArray = new float[dynamicArray.size()];
		
		for (int i=0; i<dynamicArray.size(); i++) {
			vertexDataArray[i]=dynamicArray.get(i);
		}
		
		mvbo.setVertexDataArray(vertexDataArray);
		
		// create our vertex buffer objects
	    IntBuffer buffer = BufferUtils.createIntBuffer(1);
	    GL15.glGenBuffers(buffer);
	    
	    int vertex_buffer_id = buffer.get(0);
	    
	    FloatBuffer vertex_buffer_data = BufferUtils.createFloatBuffer(vertexDataArray.length);
	    vertex_buffer_data.put(vertexDataArray);
	    vertex_buffer_data.rewind();
	    
	    mvbo.setVertexBufferId(vertex_buffer_id);
	    mvbo.setVertexBufferData(vertex_buffer_data);
		
		return mvbo;
	}
}
