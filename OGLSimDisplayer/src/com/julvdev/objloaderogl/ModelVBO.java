package com.julvdev.objloaderogl;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Vector3f;

public class ModelVBO {
	private float[] vertexDataArray;
	private int vertex_buffer_id;
	private FloatBuffer vertex_buffer_data;
	
	public ModelVBO() {
		
	}
	
	public float[] getVertexDataArray() {
		return vertexDataArray;
	}
	
	public void setVertexDataArray(float[] data) {
		vertexDataArray = data;
	}
	
	public void setVertexBufferId(int vertexBufferId) {
		vertex_buffer_id = vertexBufferId;
	}
	
	public void setVertexBufferData(FloatBuffer vertexBufferData) {
		vertex_buffer_data = vertexBufferData;
	}
	
	public void render(Vector3f position, Vector3f rotDir, float angle) {
		// link this object buffer to the current GPU buffer
	    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertex_buffer_id);
	    GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertex_buffer_data, GL15.GL_STATIC_DRAW);
	    
		GL11.glPushMatrix();
		GL11.glTranslatef(position.x, position.y, position.z); // On place l'objet par rapport au reste de la scène
		GL11.glRotatef(angle, rotDir.x, rotDir.y, rotDir.z);
		
		// render the object
        GL11.glVertexPointer(3, GL11.GL_FLOAT, 40, 0);
        GL11.glNormalPointer(GL11.GL_FLOAT, 40, 12);
        GL11.glColorPointer(4, GL11.GL_FLOAT, 40, 24);

        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vertexDataArray.length / 10);

        // restore the matrix to pre-transformation values
        GL11.glPopMatrix();
	}
}
